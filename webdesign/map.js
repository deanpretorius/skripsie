      // Initialize and add the map
      function initMap() {
        // The location of Uluru
        var node = {lat: -33.944750, lng: 18.895365};
        var testnode = {lat: -33.9338001, lng: 18.8683091};

        // The map, centered at Uluru
        var map = new google.maps.Map(
            document.getElementById('map'), {zoom: 15, center: node});
        // The marker, positioned at Uluru
        var marker = new google.maps.Marker({position: node, map: map});
        var testmarker = new google.maps.Marker({position: testnode, map: map});
      }