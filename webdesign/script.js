const firebaseConfig = {
    apiKey: "AIzaSyBUh9y7zny1fmne326rpNPvi5W8F_0-bnI",
    authDomain: "skripsie-fdac2.firebaseapp.com",
    databaseURL: "https://skripsie-fdac2.firebaseio.com",
    projectId: "skripsie-fdac2",
    storageBucket: "skripsie-fdac2.appspot.com",
    messagingSenderId: "493996181503",
    appId: "1:493996181503:web:621d3a305bd3b6dc9a09ca"
  };

// Initialize Firebase
firebase.initializeApp(firebaseConfig);

let database = firebase.database();

function writeUserData(userId, name, email, imageUrl) {
  firebase.database().ref('users/' + userId).set({
    username: name,
    email: email,
    profile_picture : imageUrl
  });
}

// writeUserData("bob2", "bab", "test@sun.ac", "hello")
var depth = [];
var counter = [];
var depthInNumber = []
var time = [];
var temp = [];
var tempInNumber = [];

let data = firebase.database().ref('/Node01').once('value').then(apples => cb(apples.val()));
function cb(data){
     //console.log(data);

     var newData = Object.values(data);     
     //newData is now an array of objects
    //   console.log(newData);


     var number =0;
    //  for (var i =0; i<100; i++){
    //     arr.push(newData[i].depth);
    //  }

    newData.forEach(function (i){
        depth.push(i.Depth);  //Can access parts of the object
        time.push(i.Time)
        temp.push(i.Temperature.replace(/\D/g,''));
        counter.push(number);
        number++;
    })
    depthInNumber = depth.map(Number);
    // temp = temp.replace(/\D/g,'');
     console.log(tempInNumber);
     console.log(time);
     console.log(temp);
     
     
     //var newData = data.products.map(function(d){ return d.depth})
     depthChart();
     tempChart();

}



function depthChart(){
    
    var depthConfig = {
        type: 'line',
        data: {
             labels: time,
             //labels: [0, 1, 3],
            datasets: [{
                label: 'Water Depth',
                data: depthInNumber,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };

var depthCTX = document.getElementById('depthChart').getContext('2d');
var depthChart = new Chart(depthCTX,  depthConfig);
};

function tempChart(){
    
    var tempConfig = {
        type: 'line',
        data: {
             labels: time,
             //labels: [0, 1, 3],
            datasets: [{
                label: 'Temperature',
                data: temp,
                backgroundColor: [
                    'rgba(255, 99, 132, 0.2)',
                    'rgba(54, 162, 235, 0.2)',
                    'rgba(255, 206, 86, 0.2)',
                    'rgba(75, 192, 192, 0.2)',
                    'rgba(153, 102, 255, 0.2)',
                    'rgba(255, 159, 64, 0.2)'
                ],
                borderColor: [
                    'rgba(255, 99, 132, 1)',
                    'rgba(54, 162, 235, 1)',
                    'rgba(255, 206, 86, 1)',
                    'rgba(75, 192, 192, 1)',
                    'rgba(153, 102, 255, 1)',
                    'rgba(255, 159, 64, 1)'
                ],
                borderWidth: 1
            }]
        },
        options: {
            scales: {
                yAxes: [{
                    ticks: {
                        beginAtZero: true
                    }
                }]
            }
        }
    };

var tempCTX = document.getElementById('tempChart').getContext('2d');
var tempChart = new Chart(tempCTX, tempConfig);
};

// TODO:
// promise
