#include <SPI.h>
#include <LoRa.h>
#include <Arduino.h>
#include "Statistic.h"
#include <ArduinoLowPower.h>


void fsm();
void test();

//LoRa functions
  void loraSetup();
  void loraSend(String message);

//Decalarations for FSM
  int counter =0;
  String message;

  ///Taking statistical measurements
  Statistic myStatsTemp;
  Statistic myStatsDepth;
  Statistic myStatsBat;
  int raw;
  float voltage;

  int raw1;
  float voltage1;
  int sensorVal1 =-1;

  enum fsmState{
    START, SLEEP, MEASURE, SEND, SENSORINIT
  };
  fsmState state  = START;
  
  int sensorVal =-1;
  int arrayCount=0;
  int sensorArray[10];
  float tempAvg =0;
  float depthAvg =0;
  float batAvg=0;

//Setting names of pins used
  // These constants won't change. They're used to give names to the pins used:
  const int depthPin = A1; // Analog input pin that the potentiometer is attached to
  const int tempPin = A2;     //Temperature input pin
  const int batPin=A3;        //Battery level sensing
  
  const int tempPower=2;      //Digital pin used to toggle temp sensor
  const int relayPin=0;       //Digital pin used to drive relay switch for transducer
  const int sleepPin=7;       //Digital pin used to enter sleep pinMode

  const int green=6;          //Digital pins for LEDs
  const int red=4;
  const int yellow=5;

  const int tamper=1;         //Pin for external tamper switch

// Test function declarations
  int depthRaw = 0;        // value read from the pot
  float depthVoltage=0;
  float depth = 0;

  int tempRaw=0;
  float tempVoltage=0;
  float temp=0;

  int batRaw=0;
  float batVoltage=0;

void setup() {
  // //Set all digital pins as outputs
      for (int i = 0; i <= 6; i++) {    //All digital pins are output pins except for the tamper switch
        pinMode(i, OUTPUT);
        digitalWrite(i, LOW);   //Pull all digital pins low at startup
      }
  //All analogue pins are initialised as input pins. So I do not have to configure them

  // pinMode(sleepPin, OUTPUT);
  // digitalWrite(sleepPin, LOW);  
  Serial.begin(115200);
  loraSetup();
}

int count =0;
void loop() {
  // put your main code here, to run repeatedly:
   fsm();
  //test();

  //Blink
    //   pinMode(7, LOW);

    // digitalWrite(6, HIGH);   // turn the LED on (HIGH is the voltage level)
    // delay(1000);                       // wait for a second
    // digitalWrite(6, LOW);    // turn the LED off by making the voltage LOW
    // delay(1000);      // wait for a second

    // count++;
    // if(count==5){
    //   digitalWrite(7, HIGH);
    //   delay(1000);
    //   count=0;
    // }

  }
int msgcounter=0;
float V;
void test(){
//initialise sensors
  digitalWrite(sleepPin, LOW);
  digitalWrite(tempPower, HIGH);
  digitalWrite(green, HIGH);
  delay(500);

//make and display sensor data
    depthRaw = analogRead(depthPin);
    depthVoltage = depthRaw * (3.3/1023.0);
    depth = 1.4865*depthVoltage -0.89195;
    // print the results to the Serial Monitor:
    Serial.print("Raw depth = ");
    Serial.print(depthRaw);
        
    Serial.print("\t Depth Voltage = ");
    Serial.print(depthVoltage);

    Serial.print("\t depth = ");
    Serial.print(depth);

    Serial.print("\n");

    tempRaw = analogRead(tempPin);
    tempVoltage = tempRaw * (3.3/1023.0);
    temp = (tempVoltage - 0.5) /0.01 ; //Subtract the Voltage offset, and then divide by 0.01mv according to temperature scale on datasheet

    // print the results to the Serial Monitor:
    Serial.print("Raw temp = ");
    Serial.print(tempRaw);
    Serial.print("\t tempVoltage = ");
    Serial.print(tempVoltage);
      
    Serial.print("\t temperature = ");
    Serial.print(temp);

    Serial.print("\n");

    batRaw = analogRead(batPin);
    batVoltage = batRaw * (3.3/1023.0) * 5.84;
    // V = batVoltage * 6.7796;
    // V = batVoltage * 5.84; 

    Serial.print("Raw battery = ");
    Serial.print(batRaw);
    Serial.print("\t Pin Voltage = ");
    Serial.print(batVoltage);
    // Serial.print("\t Battery Voltage = ");
    // Serial.print(V);

    Serial.print("\n");

    //Construct LoRa Message
        message = "$ Node01 ";
        message += msgcounter;
        message += " ";
        message += tempAvg;
        message += " ";
        message += depthAvg;
        message += " ";
        message += batAvg;
        message += " ";
        //message += status;
      
        message += " $  \n";
        Serial.print("Sending packet \n");
      // Send packet
        loraSend(message);


//Sleep
  digitalWrite(tempPower, LOW);
  digitalWrite(green, LOW);
  digitalWrite(sleepPin, HIGH);
  

  // wait 2 milliseconds before the next loop for the analog-to-digital
  // converter to settle after the last reading:
  delay(1000 *3);
  
  //delay(2000);
  // LoRa.sleep();
  // LowPower.deepSleep(2000);
}

void fsm(){
  switch(state){    
    case START:
      state = SENSORINIT;
    break;

    case SENSORINIT:
      //turn on relay for transducer
      //set pin high for mcp9700
        digitalWrite(tempPower, HIGH);
        digitalWrite(green, HIGH);
        digitalWrite(relayPin, HIGH);
        delay(1000 * 5);
        digitalWrite(green, LOW);

      state = MEASURE;
    break;

    case MEASURE:
      digitalWrite(yellow, HIGH);

      if (myStatsDepth.count() < 10){
        //Depth Sensor measurements
          depthRaw = analogRead(depthPin);
          depthVoltage = depthRaw * (3.3/1023.0);
          depth = 1.4865*depthVoltage -0.89195;
          myStatsDepth.add(depth);

        //Temperature sensor measurements
          tempRaw = analogRead(tempPin);
          tempVoltage = tempRaw * (3.3/1023.0);
          temp = (tempVoltage - 0.5) /0.01 ; //Subtract the Voltage offset, and then divide by 0.01mv according to temperature scale on datasheet
          myStatsTemp.add(temp);

        //Take battery measurements
          batRaw = analogRead(batPin);
          batVoltage = batRaw * (3.3/1023.0) * 5.84;

          myStatsBat.add(batVoltage);
        state = MEASURE;
        delay(1000);
        
        break;
      }
      else
      {
        //Calculate moivng means
          tempAvg = myStatsTemp.average();
          depthAvg = myStatsDepth.average();
          batAvg = myStatsBat.average();

          myStatsDepth.clear();
          myStatsTemp.clear();
          myStatsBat.clear();

        state = SEND;
        //state = SLEEP;
      }
    break;

    case SEND:

      // print the results to the Serial Monitor:
        Serial.print("\n \t Moving Mean of depth = ");
        Serial.print(depthAvg);
        Serial.print("\n");

        Serial.print("\t Moving Mean of temperature = ");
        Serial.print(tempAvg);
        Serial.print("\n");

        Serial.print("\t Moving Mean of battery = ");
        Serial.print(batAvg);
        Serial.print("\n");

      //Construct LoRa Message
        message = "\n$ Node01 ";
        message += msgcounter;
        message += " ";
        message += tempAvg;
        message += " ";
        message += depthAvg;
        message += " ";
        message += batAvg;
        message += " ";
        //message += status;
      
        message += " $  ";
        Serial.print("Sending packet \n");
      // Send packet
        loraSend(message);
        msgcounter++;
      state = SLEEP;
      digitalWrite(red, HIGH);
      delay(500);
      digitalWrite(red, LOW);      
      //TODO: Think about status variable!
    break;
    
    case SLEEP:
    
    digitalWrite(tempPower, LOW);
    digitalWrite(green, LOW);
    digitalWrite(relayPin, LOW);

    pinMode(sleepPin, OUTPUT);  
    digitalWrite(sleepPin, HIGH);


  // while (1) {
  //   digitalWrite(sleepPin, HIGH);
  //   delay(1);
  //   digitalWrite(sleepPin, LOW);
  //   delay(1);
  // }


    delay(1000 * 60 * 15);
    //digitalWrite(sleepPin, LOW);


   //LoRa.sleep();
   //LowPower.deepSleep(1000 * 60 * 10);
    state = SENSORINIT;
    break;

  }
}

void loraSetup()
{
  Serial.println("LoRa Sender");
  while (!LoRa.begin(868100000))
  {
    Serial.println("Starting LoRa failed!");
    delay(3000);
  }
  LoRa.setTxPower(15);
  LoRa.setSignalBandwidth(125E3);
  LoRa.setSpreadingFactor(7);
  LoRa.setSignalBandwidth(125E3);
  LoRa.setFrequency(8681E5);
  //in the tranceiver code, it is set to 34 and not 39.
  //arduino LoRa api sets this to 0x12 default
  LoRa.setPreambleLength(8);
  LoRa.enableCrc();
  LoRa.setCodingRate4(8);
  LoRa.setSyncWord(0x12);
}

void loraSend(String message)
{
  if (LoRa.beginPacket())
  {
    LoRa.print(message);
    //LoRa.print(String(counter));

    LoRa.endPacket();
    Serial.print("Message sent \n");
  }
  else
  {
    Serial.print("Message not sent");
  }
}
